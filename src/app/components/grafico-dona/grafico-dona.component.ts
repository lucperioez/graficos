import { Component, Input, OnInit } from '@angular/core';
import { ChartType } from 'angular-google-charts';
import { Options } from 'src/app/models/chartOption.model';

@Component({
  selector: 'app-grafico-dona',
  templateUrl: './grafico-dona.component.html',
  styleUrls: ['./grafico-dona.component.scss']
})
export class GraficoDonaComponent implements OnInit {
  @Input() options!:Options; 
  type: ChartType = ChartType.PieChart;
  donutChart:any;
  
  width = 0;  
  height = 300; 

  constructor() {
    this.width = screen.width;
   }

  ngOnInit(): void {
    
    console.log(this.options);
    this.donutChart = 
    {
      type:this.type,
      data:this.options.data,
      options:{     
        pieHole: 0.9,
        backgroundColor:'none',
        legend: 'none',
        pieSliceText: 'none',
        colors:this.options.colors ,
      }
    }
    
    
  }


}
