import { Component, Input, OnInit } from '@angular/core';
import { ChartType } from 'angular-google-charts';
import { Options } from 'src/app/models/chartOption.model';

@Component({
  selector: 'app-grafico-area',
  templateUrl: './grafico-area.component.html',
  styleUrls: ['./grafico-area.component.scss']
})
export class GraficoAreaComponent implements OnInit {
  type: ChartType = ChartType.AreaChart;
  width = 209;  
  height = 70; 
  areaChart:any;
  @Input() options!:Options; 
  constructor() { }

  ngOnInit(): void {

    this.areaChart= {
      type:this.type,
      historyData:this.options.data,
    
      historyColumnNames: ['Category', 'Percentage'],
    
      historyOptions: {     
        pieHole: 0.9,
        legend: 'none',
        backgroundColor:'none',
        pieSliceText: 'none',
        colors:this.options.colors ,
        vAxis: {
          gridlines: {
            interval: 0
          },  
            baselineColor: '#fff',
            gridlineColor: '#fff',
            textPosition: 'none'
      
        },
        hAxis: {  
          gridlines: {
            interval: 0
          },
          baselineColor: '#fff',
          gridlineColor: '#fff',
          textPosition: 'none'      
        },
        
      }
    }
  }

}
