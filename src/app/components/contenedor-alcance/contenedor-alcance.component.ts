import { Component, Input,  OnInit } from '@angular/core';
import { ChartType } from 'angular-google-charts';
import { ChartOptions } from 'src/app/models/chartOption.model';
import {ChartServiceService  } from "../../services/chart-service.service";

@Component({
  selector: 'app-grafico',
  templateUrl: './contenedor-alcance.component.html',
  styleUrls: ['./contenedor-alcance.component.scss'],
})

export class GraficoComponent implements OnInit {
  @Input() chartOptions:any; 
  componentOptions:ChartOptions;
  type: ChartType = ChartType.PieChart;
  donutChart:any;
  areaChart:any;
  tabletValue:number;
  smartPhoneValue:number;
  historyType: ChartType = ChartType.AreaChart;
  width = 500;  
  height = 300;  
  width2 = 210;  
  height2 = 70; 
  colorType1!:string;
  colorType2!:string;
  totalValue = 0;
  
  constructor(private chartServiceService: ChartServiceService){
    console.log(this.chartOptions);
    this.componentOptions = {
      symbol:"",
      title:"",
      dounutChart:{ 
        smartphonePercent:0,
        tabletPercent:0,
        totalValue:0,
        chartData:{
        data:[],
        colors:[]
    }
      },
        areaChart:{ 
          chartData:{
            data:[],
            colors:[]
          }
          
      }
    };
    
    this.tabletValue = 0;
    this.smartPhoneValue = 0;
    
  }

  calculatePercentage(total:number, percent:number){
    return ( total * percent) / 100;

  }

  

  randomChartValues(){
    var array:number[][] = [];
    for (let index = 0; index < 20; index++) {
      let randomValue = Math.floor(Math.random() * (20 - 1 + 1) + 1)
      array.push([index,randomValue]);
    }
    return array;
  }

  getNewData(){
    this.chartServiceService.getNewData().then(res=>{
    let response:any = res;

    this.areaChart.historyData = response.chartNewData;
    let total = this.chartOptions.dounutChart.totalValue;
    this.chartOptions.dounutChart.tabletPercent = response.tabletValue;
    this.chartOptions.dounutChart.smartphonePercent = (100 - response.tabletValue);
    this.tabletValue = this.calculatePercentage(total,response.tabletValue);
    this.donutChart.data = [
      ['Smartphone',   this.chartOptions.dounutChart.smartphonePercent],
      ['Tablet',    this.chartOptions.dounutChart.tabletPercent], 
    ];
    console.log(total);
    console.log((100 - response.tabletValue));
    this.smartPhoneValue = this.calculatePercentage(total,(100 - response.tabletValue));

    })
  }
  

  ngOnInit() { 
    let total = this.chartOptions.dounutChart.totalValue;
    this.componentOptions = this.chartOptions;
    this.tabletValue = this.calculatePercentage(total,this.chartOptions.dounutChart.tabletPercent);
    this.smartPhoneValue = this.calculatePercentage(total,this.chartOptions.dounutChart.smartphonePercent);
    
    this.colorType1 = this.chartOptions.dounutChart.chartData.colors[1];
    this.colorType2 = this.chartOptions.dounutChart.chartData.colors[0];
  }  
  

  getNew(){
    this.getNewData();
  }


}
