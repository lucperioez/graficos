import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ChartServiceService {

  constructor() { }


  randomChartPercent(){
    return Math.floor(Math.random()*100) + 1;
  }

  randomChartValues(){
    var array:number[][] = [];
    for (let index = 0; index < 20; index++) {
      let randomValue = Math.floor(Math.random() * (20 - 1 + 1) + 1)
      array.push([index,randomValue]);
    }
    return array;
  }

  getNewData(){
    let chartNewData = this.randomChartValues();

    return new Promise((resolve, reject) => {resolve(
      {
        chartNewData,
        smartPhoneValue:this.randomChartPercent(),
        tabletValue:this.randomChartPercent()
      }
    ) });
  }


}
