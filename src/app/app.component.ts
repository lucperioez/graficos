import { Component, ViewChild } from '@angular/core';
import { GraficoComponent } from './components/contenedor-alcance/contenedor-alcance.component';
import { ChartOptions } from "./models/chartOption.model";
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'graficos';
  historyData!:number[][]; 
  revenueChartoption!:ChartOptions; 
  impresionsChartoption!:ChartOptions; 
  visitsChartoption:ChartOptions; 
  @ViewChild('graficoComponent1')graficoComponent1!:GraficoComponent;
  @ViewChild('graficoComponent2')graficoComponent2!:GraficoComponent;
  @ViewChild('graficoComponent3')graficoComponent3!:GraficoComponent;


  donutChartOption!:any;
  areaChartOption!:any;

  constructor(){
    this.revenueChartoption = {
      title:"REVENUE",
      symbol:'€',
      dounutChart:{ 
        smartphonePercent:40,
        tabletPercent:60,
        totalValue:200000,
        chartData:{
          data:[
            ['Smartphone',40],
            ['Tablet',60]
          ],
          colors:["#376619","#85D03D"]
        }
      },
        areaChart:{ 
          chartData:{
          data:this.randomChartValues(),
          colors:["#376619"]
          }
      }
    }
    this.impresionsChartoption = {
      title:"IMPRESIONS",
      symbol:'',
      dounutChart:{ 
        smartphonePercent:60,
        tabletPercent:40,
        totalValue:50000000,
         chartData:{
          data:[
            ['Smartphone',60],
            ['Tablet',40]
          ],
          colors:["#2A4E59","#76C6D9"]
        }
      },
        areaChart:{ 
          chartData:{
            data:this.randomChartValues(),
            colors:["#2A4E59"]
          }
      }
    } 
    this.visitsChartoption = {
      title:"VISITS",
      symbol:'',
      dounutChart:{ 
        smartphonePercent:20,
        tabletPercent:80,
        totalValue:60000000,
        chartData:{
          data:[
            ['Smartphone',40],
            ['Tablet',60]
          ],
          colors:["#B8521E","#EABF2D"]
        }
      },
        areaChart:{ 
          chartData:{
          data:this.randomChartValues(),
          colors:["#EABF2D"]
        }
      },

    }


  }

  randomChartValues(){
    var array:number[][] = [];
    for (let index = 0; index < 20; index++) {
      let randomValue = Math.floor(Math.random() * (20 - 1 + 1) + 1)
      array.push([index,randomValue]);
    }
    return array;
  }
  
  refresh(){
    this.graficoComponent1.getNew();
    this.graficoComponent2.getNew();
    this.graficoComponent3.getNew();
  }
}
