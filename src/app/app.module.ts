import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { GoogleChartsModule } from 'angular-google-charts';
import { AppComponent } from './app.component';
import { GraficoComponent } from './components/contenedor-alcance/contenedor-alcance.component';
import { PointReplacerPipe } from './pipe/point-replacer.pipe';
import { GraficoDonaComponent } from './components/grafico-dona/grafico-dona.component';
import { GraficoAreaComponent } from './components/grafico-area/grafico-area.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';

@NgModule({
  declarations: [
    AppComponent,
    GraficoComponent,
    PointReplacerPipe,
    GraficoDonaComponent,
    GraficoAreaComponent,
  ],
  imports: [
    BrowserModule,
    GoogleChartsModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule

  ],
  providers: [PointReplacerPipe],
  bootstrap: [AppComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }
