export interface DounutChart
{ 
   
    smartphonePercent:number,
    tabletPercent:number,
    totalValue:number,
    chartData:{
        data:any,
        colors:string[]
    }
}
export interface AreaChart
{ 
    chartData:{
        data:any,
        colors:string[]
    }
}
export interface ChartOptions
{ 
    symbol:string,
    title:string,
    dounutChart:DounutChart
    areaChart:AreaChart
}

export interface Options
{ 
    data:number[][],
    colors:string[]
}

