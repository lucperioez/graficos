(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"], {
    /***/
    0:
    /*!***************************!*\
      !*** multi ./src/main.ts ***!
      \***************************/

    /*! no static exports found */

    /***/
    function _(module, exports, __webpack_require__) {
      module.exports = __webpack_require__(
      /*! C:\Users\lucio\Downloads\graficos\src\main.ts */
      "zUnb");
      /***/
    },

    /***/
    "1698":
    /*!***************************************************!*\
      !*** ./src/app/services/chart-service.service.ts ***!
      \***************************************************/

    /*! exports provided: ChartServiceService */

    /***/
    function _(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ChartServiceService", function () {
        return ChartServiceService;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var ChartServiceService = /*#__PURE__*/function () {
        function ChartServiceService() {
          _classCallCheck(this, ChartServiceService);
        }

        _createClass(ChartServiceService, [{
          key: "randomChartPercent",
          value: function randomChartPercent() {
            return Math.floor(Math.random() * 100) + 1;
          }
        }, {
          key: "randomChartValues",
          value: function randomChartValues() {
            var array = [];

            for (var index = 0; index < 20; index++) {
              var randomValue = Math.floor(Math.random() * (20 - 1 + 1) + 1);
              array.push([index, randomValue]);
            }

            return array;
          }
        }, {
          key: "getNewData",
          value: function getNewData() {
            var _this = this;

            var chartNewData = this.randomChartValues();
            return new Promise(function (resolve, reject) {
              resolve({
                chartNewData: chartNewData,
                smartPhoneValue: _this.randomChartPercent(),
                tabletValue: _this.randomChartPercent()
              });
            });
          }
        }]);

        return ChartServiceService;
      }();

      ChartServiceService.ɵfac = function ChartServiceService_Factory(t) {
        return new (t || ChartServiceService)();
      };

      ChartServiceService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
        token: ChartServiceService,
        factory: ChartServiceService.ɵfac,
        providedIn: 'root'
      });
      /***/
    },

    /***/
    "1zoH":
    /*!*******************************************************************!*\
      !*** ./src/app/components/grafico-area/grafico-area.component.ts ***!
      \*******************************************************************/

    /*! exports provided: GraficoAreaComponent */

    /***/
    function zoH(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "GraficoAreaComponent", function () {
        return GraficoAreaComponent;
      });
      /* harmony import */


      var angular_google_charts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! angular-google-charts */
      "icpI");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var GraficoAreaComponent = /*#__PURE__*/function () {
        function GraficoAreaComponent() {
          _classCallCheck(this, GraficoAreaComponent);

          this.type = angular_google_charts__WEBPACK_IMPORTED_MODULE_0__["ChartType"].AreaChart;
          this.width = 209;
          this.height = 70;
        }

        _createClass(GraficoAreaComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.areaChart = {
              type: this.type,
              historyData: this.options.data,
              historyColumnNames: ['Category', 'Percentage'],
              historyOptions: {
                pieHole: 0.9,
                legend: 'none',
                backgroundColor: 'none',
                pieSliceText: 'none',
                colors: this.options.colors,
                vAxis: {
                  gridlines: {
                    interval: 0
                  },
                  baselineColor: '#fff',
                  gridlineColor: '#fff',
                  textPosition: 'none'
                },
                hAxis: {
                  gridlines: {
                    interval: 0
                  },
                  baselineColor: '#fff',
                  gridlineColor: '#fff',
                  textPosition: 'none'
                }
              }
            };
          }
        }]);

        return GraficoAreaComponent;
      }();

      GraficoAreaComponent.ɵfac = function GraficoAreaComponent_Factory(t) {
        return new (t || GraficoAreaComponent)();
      };

      GraficoAreaComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
        type: GraficoAreaComponent,
        selectors: [["app-grafico-area"]],
        inputs: {
          options: "options"
        },
        decls: 3,
        vars: 6,
        consts: [[1, "area-chart"], [3, "type", "data", "columnNames", "options", "width", "height"], ["chart2", ""]],
        template: function GraficoAreaComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "google-chart", 1, 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("type", ctx.areaChart.type)("data", ctx.areaChart.historyData)("columnNames", ctx.areaChart.historyColumnNames)("options", ctx.areaChart.historyOptions)("width", ctx.width)("height", ctx.height);
          }
        },
        directives: [angular_google_charts__WEBPACK_IMPORTED_MODULE_0__["GoogleChartComponent"]],
        styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJncmFmaWNvLWFyZWEuY29tcG9uZW50LnNjc3MifQ== */"]
      });
      /***/
    },

    /***/
    "AytR":
    /*!*****************************************!*\
      !*** ./src/environments/environment.ts ***!
      \*****************************************/

    /*! exports provided: environment */

    /***/
    function AytR(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "environment", function () {
        return environment;
      }); // This file can be replaced during build by using the `fileReplacements` array.
      // `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
      // The list of file replacements can be found in `angular.json`.


      var environment = {
        production: false
      };
      /*
       * For easier debugging in development mode, you can import the following file
       * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
       *
       * This import should be commented out in production mode because it will have a negative impact
       * on performance if an error is thrown.
       */
      // import 'zone.js/dist/zone-error';  // Included with Angular CLI.

      /***/
    },

    /***/
    "KlJu":
    /*!*********************************************!*\
      !*** ./src/app/pipe/point-replacer.pipe.ts ***!
      \*********************************************/

    /*! exports provided: PointReplacerPipe */

    /***/
    function KlJu(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PointReplacerPipe", function () {
        return PointReplacerPipe;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var PointReplacerPipe = /*#__PURE__*/function () {
        function PointReplacerPipe() {
          _classCallCheck(this, PointReplacerPipe);
        }

        _createClass(PointReplacerPipe, [{
          key: "transform",
          value: function transform(value) {
            console.log(value);
            var formatter = new Intl.NumberFormat('en-US', {
              style: 'currency',
              currency: 'EUR',
              maximumFractionDigits: 0,
              minimumFractionDigits: 0
            });
            var price = formatter.format(value);
            return String(price).replace(/,/g, '.').replace('€', '');
          }
        }]);

        return PointReplacerPipe;
      }();

      PointReplacerPipe.ɵfac = function PointReplacerPipe_Factory(t) {
        return new (t || PointReplacerPipe)();
      };

      PointReplacerPipe.ɵpipe = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefinePipe"]({
        name: "pointReplacer",
        type: PointReplacerPipe,
        pure: true
      });
      /***/
    },

    /***/
    "Sy1n":
    /*!**********************************!*\
      !*** ./src/app/app.component.ts ***!
      \**********************************/

    /*! exports provided: AppComponent */

    /***/
    function Sy1n(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppComponent", function () {
        return AppComponent;
      });
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/material/toolbar */
      "/t3+");
      /* harmony import */


      var _components_contenedor_alcance_contenedor_alcance_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./components/contenedor-alcance/contenedor-alcance.component */
      "dg0K");

      var _c0 = ["graficoComponent1"];
      var _c1 = ["graficoComponent2"];
      var _c2 = ["graficoComponent3"];

      var AppComponent = /*#__PURE__*/function () {
        function AppComponent() {
          _classCallCheck(this, AppComponent);

          this.title = 'graficos';
          this.revenueChartoption = {
            title: "REVENUE",
            symbol: '€',
            dounutChart: {
              smartphonePercent: 40,
              tabletPercent: 60,
              totalValue: 200000,
              chartData: {
                data: [['Smartphone', 40], ['Tablet', 60]],
                colors: ["#376619", "#85D03D"]
              }
            },
            areaChart: {
              chartData: {
                data: this.randomChartValues(),
                colors: ["#376619"]
              }
            }
          };
          this.impresionsChartoption = {
            title: "IMPRESIONS",
            symbol: '',
            dounutChart: {
              smartphonePercent: 60,
              tabletPercent: 40,
              totalValue: 50000000,
              chartData: {
                data: [['Smartphone', 60], ['Tablet', 40]],
                colors: ["#2A4E59", "#76C6D9"]
              }
            },
            areaChart: {
              chartData: {
                data: this.randomChartValues(),
                colors: ["#2A4E59"]
              }
            }
          };
          this.visitsChartoption = {
            title: "VISITS",
            symbol: '',
            dounutChart: {
              smartphonePercent: 20,
              tabletPercent: 80,
              totalValue: 60000000,
              chartData: {
                data: [['Smartphone', 40], ['Tablet', 60]],
                colors: ["#B8521E", "#EABF2D"]
              }
            },
            areaChart: {
              chartData: {
                data: this.randomChartValues(),
                colors: ["#EABF2D"]
              }
            }
          };
        }

        _createClass(AppComponent, [{
          key: "randomChartValues",
          value: function randomChartValues() {
            var array = [];

            for (var index = 0; index < 20; index++) {
              var randomValue = Math.floor(Math.random() * (20 - 1 + 1) + 1);
              array.push([index, randomValue]);
            }

            return array;
          }
        }, {
          key: "refresh",
          value: function refresh() {
            this.graficoComponent1.getNew();
            this.graficoComponent2.getNew();
            this.graficoComponent3.getNew();
          }
        }]);

        return AppComponent;
      }();

      AppComponent.ɵfac = function AppComponent_Factory(t) {
        return new (t || AppComponent)();
      };

      AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
        type: AppComponent,
        selectors: [["app-root"]],
        viewQuery: function AppComponent_Query(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c0, 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c1, 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c2, 1);
          }

          if (rf & 2) {
            var _t;

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.graficoComponent1 = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.graficoComponent2 = _t.first);
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.graficoComponent3 = _t.first);
          }
        },
        decls: 11,
        vars: 3,
        consts: [["color", "primary"], [1, "app-container"], [3, "chartOptions"], ["graficoComponent1", ""], ["graficoComponent2", ""], ["graficoComponent3", ""]],
        template: function AppComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-toolbar", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "mat-toolbar-row");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "span");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "PRUEBA T\xC9CNICA FRONT");

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "app-grafico", 2, 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](7, "app-grafico", 2, 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "app-grafico", 2, 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("chartOptions", ctx.revenueChartoption);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("chartOptions", ctx.impresionsChartoption);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("chartOptions", ctx.visitsChartoption);
          }
        },
        directives: [_angular_material_toolbar__WEBPACK_IMPORTED_MODULE_1__["MatToolbar"], _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_1__["MatToolbarRow"], _components_contenedor_alcance_contenedor_alcance_component__WEBPACK_IMPORTED_MODULE_2__["GraficoComponent"]],
        styles: [".app-container[_ngcontent-%COMP%] {\n  display: flex;\n  flex-direction: row;\n  width: auto;\n  width: 98%;\n}\n.app-container[_ngcontent-%COMP%]   app-grafico[_ngcontent-%COMP%] {\n  width: 33%;\n  margin-bottom: 7px;\n  margin: 3px;\n  margin-right: 0.5%;\n  margin-left: 0.5%;\n}\n@media only screen and (max-width: 1218px) {\n  app-grafico[_ngcontent-%COMP%] {\n    width: 100% !important;\n  }\n\n  .app-container[_ngcontent-%COMP%] {\n    display: flex;\n    flex-direction: column;\n    width: 100% !important;\n  }\n\n  svg[_ngcontent-%COMP%] {\n    width: 200px;\n  }\n}\n.mat-toolbar-row[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: center;\n}\n.mat-toolbar[_ngcontent-%COMP%] {\n  background-color: #303f9f;\n  color: white;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcYXBwLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLFVBQUE7QUFDRjtBQUFFO0VBQ0UsVUFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7QUFFSjtBQUVBO0VBQ0U7SUFDRSxzQkFBQTtFQUNGOztFQUNBO0lBQ0UsYUFBQTtJQUNBLHNCQUFBO0lBQ0Esc0JBQUE7RUFFRjs7RUFBQTtJQUNFLFlBQUE7RUFHRjtBQUNGO0FBQUE7RUFDRSxhQUFBO0VBQ0EsdUJBQUE7QUFFRjtBQUNBO0VBQ0UseUJBQUE7RUFDQSxZQUFBO0FBRUYiLCJmaWxlIjoiYXBwLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmFwcC1jb250YWluZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICB3aWR0aDogYXV0bztcbiAgd2lkdGg6IDk4JTtcbiAgYXBwLWdyYWZpY28ge1xuICAgIHdpZHRoOiAzMyU7XG4gICAgbWFyZ2luLWJvdHRvbTogN3B4O1xuICAgIG1hcmdpbjogM3B4O1xuICAgIG1hcmdpbi1yaWdodDogMC41JTtcbiAgICBtYXJnaW4tbGVmdDogMC41JTtcbiAgfVxufVxuXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDEyMThweCkge1xuICBhcHAtZ3JhZmljbyB7XG4gICAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbiAgfVxuICAuYXBwLWNvbnRhaW5lciB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XG4gIH1cbiAgc3ZnIHtcbiAgICB3aWR0aDogMjAwcHg7XG4gIH1cbn1cblxuLm1hdC10b29sYmFyLXJvd3tcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG5cbi5tYXQtdG9vbGJhcntcbiAgYmFja2dyb3VuZC1jb2xvcjogIzMwM2Y5ZjtcbiAgY29sb3I6IHdoaXRlO1xufVxuIl19 */"]
      });
      /***/
    },

    /***/
    "ZAI4":
    /*!*******************************!*\
      !*** ./src/app/app.module.ts ***!
      \*******************************/

    /*! exports provided: AppModule */

    /***/
    function ZAI4(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AppModule", function () {
        return AppModule;
      });
      /* harmony import */


      var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/platform-browser */
      "jhN1");
      /* harmony import */


      var angular_google_charts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! angular-google-charts */
      "icpI");
      /* harmony import */


      var _app_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./app.component */
      "Sy1n");
      /* harmony import */


      var _components_contenedor_alcance_contenedor_alcance_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./components/contenedor-alcance/contenedor-alcance.component */
      "dg0K");
      /* harmony import */


      var _pipe_point_replacer_pipe__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./pipe/point-replacer.pipe */
      "KlJu");
      /* harmony import */


      var _components_grafico_dona_grafico_dona_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./components/grafico-dona/grafico-dona.component */
      "zNhO");
      /* harmony import */


      var _components_grafico_area_grafico_area_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./components/grafico-area/grafico-area.component */
      "1zoH");
      /* harmony import */


      var _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @angular/material/toolbar */
      "/t3+");
      /* harmony import */


      var _angular_material_icon__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @angular/material/icon */
      "NFeN");
      /* harmony import */


      var _angular_material_card__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! @angular/material/card */
      "Wp6s");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var AppModule = /*#__PURE__*/_createClass(function AppModule() {
        _classCallCheck(this, AppModule);
      });

      AppModule.ɵfac = function AppModule_Factory(t) {
        return new (t || AppModule)();
      };

      AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵdefineNgModule"]({
        type: AppModule,
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"]]
      });
      AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵdefineInjector"]({
        providers: [_pipe_point_replacer_pipe__WEBPACK_IMPORTED_MODULE_4__["PointReplacerPipe"]],
        imports: [[_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"], angular_google_charts__WEBPACK_IMPORTED_MODULE_1__["GoogleChartsModule"], _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_7__["MatToolbarModule"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_8__["MatIconModule"], _angular_material_card__WEBPACK_IMPORTED_MODULE_9__["MatCardModule"]]]
      });

      (function () {
        (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_10__["ɵɵsetNgModuleScope"](AppModule, {
          declarations: [_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"], _components_contenedor_alcance_contenedor_alcance_component__WEBPACK_IMPORTED_MODULE_3__["GraficoComponent"], _pipe_point_replacer_pipe__WEBPACK_IMPORTED_MODULE_4__["PointReplacerPipe"], _components_grafico_dona_grafico_dona_component__WEBPACK_IMPORTED_MODULE_5__["GraficoDonaComponent"], _components_grafico_area_grafico_area_component__WEBPACK_IMPORTED_MODULE_6__["GraficoAreaComponent"]],
          imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"], angular_google_charts__WEBPACK_IMPORTED_MODULE_1__["GoogleChartsModule"], _angular_material_toolbar__WEBPACK_IMPORTED_MODULE_7__["MatToolbarModule"], _angular_material_icon__WEBPACK_IMPORTED_MODULE_8__["MatIconModule"], _angular_material_card__WEBPACK_IMPORTED_MODULE_9__["MatCardModule"]]
        });
      })();
      /***/

    },

    /***/
    "dg0K":
    /*!*******************************************************************************!*\
      !*** ./src/app/components/contenedor-alcance/contenedor-alcance.component.ts ***!
      \*******************************************************************************/

    /*! exports provided: GraficoComponent */

    /***/
    function dg0K(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "GraficoComponent", function () {
        return GraficoComponent;
      });
      /* harmony import */


      var angular_google_charts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! angular-google-charts */
      "icpI");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _services_chart_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ../../services/chart-service.service */
      "1698");
      /* harmony import */


      var _angular_material_card__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/material/card */
      "Wp6s");
      /* harmony import */


      var _grafico_dona_grafico_dona_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../grafico-dona/grafico-dona.component */
      "zNhO");
      /* harmony import */


      var _grafico_area_grafico_area_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../grafico-area/grafico-area.component */
      "1zoH");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/common */
      "ofXK");
      /* harmony import */


      var _pipe_point_replacer_pipe__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! ../../pipe/point-replacer.pipe */
      "KlJu");

      var _c0 = function _c0(a0) {
        return {
          "color": a0
        };
      };

      var GraficoComponent = /*#__PURE__*/function () {
        function GraficoComponent(chartServiceService) {
          _classCallCheck(this, GraficoComponent);

          this.chartServiceService = chartServiceService;
          this.type = angular_google_charts__WEBPACK_IMPORTED_MODULE_0__["ChartType"].PieChart;
          this.historyType = angular_google_charts__WEBPACK_IMPORTED_MODULE_0__["ChartType"].AreaChart;
          this.width = 500;
          this.height = 300;
          this.width2 = 210;
          this.height2 = 70;
          this.totalValue = 0;
          console.log(this.chartOptions);
          this.componentOptions = {
            symbol: "",
            title: "",
            dounutChart: {
              smartphonePercent: 0,
              tabletPercent: 0,
              totalValue: 0,
              chartData: {
                data: [],
                colors: []
              }
            },
            areaChart: {
              chartData: {
                data: [],
                colors: []
              }
            }
          };
          this.tabletValue = 0;
          this.smartPhoneValue = 0;
        }

        _createClass(GraficoComponent, [{
          key: "calculatePercentage",
          value: function calculatePercentage(total, percent) {
            return total * percent / 100;
          }
        }, {
          key: "randomChartValues",
          value: function randomChartValues() {
            var array = [];

            for (var index = 0; index < 20; index++) {
              var randomValue = Math.floor(Math.random() * (20 - 1 + 1) + 1);
              array.push([index, randomValue]);
            }

            return array;
          }
        }, {
          key: "getNewData",
          value: function getNewData() {
            var _this2 = this;

            this.chartServiceService.getNewData().then(function (res) {
              var response = res;
              _this2.areaChart.historyData = response.chartNewData;
              var total = _this2.chartOptions.dounutChart.totalValue;
              _this2.chartOptions.dounutChart.tabletPercent = response.tabletValue;
              _this2.chartOptions.dounutChart.smartphonePercent = 100 - response.tabletValue;
              _this2.tabletValue = _this2.calculatePercentage(total, response.tabletValue);
              _this2.donutChart.data = [['Smartphone', _this2.chartOptions.dounutChart.smartphonePercent], ['Tablet', _this2.chartOptions.dounutChart.tabletPercent]];
              console.log(total);
              console.log(100 - response.tabletValue);
              _this2.smartPhoneValue = _this2.calculatePercentage(total, 100 - response.tabletValue);
            });
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {
            var total = this.chartOptions.dounutChart.totalValue;
            this.componentOptions = this.chartOptions;
            this.tabletValue = this.calculatePercentage(total, this.chartOptions.dounutChart.tabletPercent);
            this.smartPhoneValue = this.calculatePercentage(total, this.chartOptions.dounutChart.smartphonePercent);
            this.colorType1 = this.chartOptions.dounutChart.chartData.colors[1];
            this.colorType2 = this.chartOptions.dounutChart.chartData.colors[0];
          }
        }, {
          key: "getNew",
          value: function getNew() {
            this.getNewData();
          }
        }]);

        return GraficoComponent;
      }();

      GraficoComponent.ɵfac = function GraficoComponent_Factory(t) {
        return new (t || GraficoComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_services_chart_service_service__WEBPACK_IMPORTED_MODULE_2__["ChartServiceService"]));
      };

      GraficoComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
        type: GraficoComponent,
        selectors: [["app-grafico"]],
        inputs: {
          chartOptions: "chartOptions"
        },
        decls: 31,
        vars: 23,
        consts: [[1, "main-container"], [1, "donut-chart"], [1, "header-container"], [1, "title"], [1, "total-value"], [3, "options"], [1, "area-chart"], [1, "parent"], [1, "child1"], [1, "tablet-type", 3, "ngStyle"], [1, "values"], [1, "percent"], [1, "device-amount"], [1, "child2"], [1, "smartphone-type", 3, "ngStyle"]],
        template: function GraficoComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "mat-card");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "div", 0);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 1);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "div", 2);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "span", 3);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "span", 4);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](7);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](8, "pointReplacer");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](9, "app-grafico-dona", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "div", 6);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](11, "app-grafico-area", 5);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "div", 7);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "div", 8);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "span", 9);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](15, "Tablet");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](16, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](17, "span", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](18);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](19, "span", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](20);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](21, "pointReplacer");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](22, "div", 13);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](23, "span", 14);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](24, "Smartphone");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](25, "div", 10);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](26, "span", 11);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](27);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](28, "span", 12);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](29);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](30, "pointReplacer");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("", ctx.chartOptions.title, " ");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate2"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind1"](8, 13, ctx.chartOptions.dounutChart.totalValue), " ", ctx.chartOptions.symbol, "");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("options", ctx.chartOptions.dounutChart.chartData);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("options", ctx.chartOptions.areaChart.chartData);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngStyle", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](19, _c0, ctx.colorType1));

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("", ctx.chartOptions.dounutChart.tabletPercent, "%");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate2"]("", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind1"](21, 15, ctx.tabletValue), "", ctx.chartOptions.symbol, "");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngStyle", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction1"](21, _c0, ctx.colorType2));

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("", ctx.chartOptions.dounutChart.smartphonePercent, "%");

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);

            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate2"]("", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind1"](30, 17, ctx.smartPhoneValue), "", ctx.chartOptions.symbol, "");
          }
        },
        directives: [_angular_material_card__WEBPACK_IMPORTED_MODULE_3__["MatCard"], _grafico_dona_grafico_dona_component__WEBPACK_IMPORTED_MODULE_4__["GraficoDonaComponent"], _grafico_area_grafico_area_component__WEBPACK_IMPORTED_MODULE_5__["GraficoAreaComponent"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgStyle"]],
        pipes: [_pipe_point_replacer_pipe__WEBPACK_IMPORTED_MODULE_7__["PointReplacerPipe"]],
        styles: [".main-container[_ngcontent-%COMP%] {\n  display: flex;\n  flex-direction: column;\n  width: 100%;\n}\n.main-container[_ngcontent-%COMP%]   .header-container[_ngcontent-%COMP%] {\n  display: flex;\n  flex-direction: column;\n  position: absolute;\n  margin-top: 110px;\n  width: 100%;\n  text-align: center;\n}\n.main-container[_ngcontent-%COMP%]   .header-container[_ngcontent-%COMP%]   .title[_ngcontent-%COMP%] {\n  font-size: 1.2em;\n  color: #94999e;\n}\n.main-container[_ngcontent-%COMP%]   .header-container[_ngcontent-%COMP%]   .total-value[_ngcontent-%COMP%] {\n  font-size: 1.6em;\n  color: #37393b;\n}\n.main-container[_ngcontent-%COMP%]   .donut-chart[_ngcontent-%COMP%] {\n  display: grid;\n  align-content: center;\n  justify-content: center;\n  position: relative;\n  z-index: 1;\n}\n.main-container[_ngcontent-%COMP%]   .area-chart[_ngcontent-%COMP%] {\n  margin-top: -155px;\n  display: grid;\n  align-content: center;\n  justify-content: center;\n}\n.main-container[_ngcontent-%COMP%]   .parent[_ngcontent-%COMP%] {\n  display: flex;\n  flex-direction: row;\n  width: 70%;\n  margin-right: 15%;\n  margin-left: 15%;\n  margin-top: 5%;\n  margin-bottom: 3%;\n}\n.main-container[_ngcontent-%COMP%]   .parent[_ngcontent-%COMP%]   .child1[_ngcontent-%COMP%] {\n  width: 50%;\n  margin-right: 100px;\n  display: flex;\n  flex-direction: column;\n}\n.main-container[_ngcontent-%COMP%]   .parent[_ngcontent-%COMP%]   .child1[_ngcontent-%COMP%]   .tablet-type[_ngcontent-%COMP%] {\n  font-size: 1rem;\n  font-weight: bolder;\n}\n.main-container[_ngcontent-%COMP%]   .parent[_ngcontent-%COMP%]   .child1[_ngcontent-%COMP%]   .values[_ngcontent-%COMP%] {\n  display: flex;\n  flex-direction: row;\n}\n.main-container[_ngcontent-%COMP%]   .parent[_ngcontent-%COMP%]   .child1[_ngcontent-%COMP%]   .values[_ngcontent-%COMP%]   .percent[_ngcontent-%COMP%] {\n  font-size: 1rem;\n  color: #37393b;\n}\n.main-container[_ngcontent-%COMP%]   .parent[_ngcontent-%COMP%]   .child1[_ngcontent-%COMP%]   .values[_ngcontent-%COMP%]   .device-amount[_ngcontent-%COMP%] {\n  font-size: 1rem;\n  color: #9da4ac;\n}\n.main-container[_ngcontent-%COMP%]   .parent[_ngcontent-%COMP%]   .child1[_ngcontent-%COMP%]   .values[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  margin-right: 8px;\n}\n.main-container[_ngcontent-%COMP%]   .parent[_ngcontent-%COMP%]   .child2[_ngcontent-%COMP%] {\n  text-align: right;\n  width: 50%;\n  display: flex;\n  flex-direction: column;\n}\n.main-container[_ngcontent-%COMP%]   .parent[_ngcontent-%COMP%]   .child2[_ngcontent-%COMP%]   .smartphone-type[_ngcontent-%COMP%] {\n  font-size: 1rem;\n  font-weight: bolder;\n}\n.main-container[_ngcontent-%COMP%]   .parent[_ngcontent-%COMP%]   .child2[_ngcontent-%COMP%]   .values[_ngcontent-%COMP%] {\n  display: flex;\n  flex-direction: row;\n  margin-left: auto;\n}\n.main-container[_ngcontent-%COMP%]   .parent[_ngcontent-%COMP%]   .child2[_ngcontent-%COMP%]   .values[_ngcontent-%COMP%]   .percent[_ngcontent-%COMP%] {\n  font-size: 1rem;\n  color: #37393b;\n}\n.main-container[_ngcontent-%COMP%]   .parent[_ngcontent-%COMP%]   .child2[_ngcontent-%COMP%]   .values[_ngcontent-%COMP%]   .device-amount[_ngcontent-%COMP%] {\n  font-size: 1rem;\n  color: #9da4ac;\n}\n.main-container[_ngcontent-%COMP%]   .parent[_ngcontent-%COMP%]   .child2[_ngcontent-%COMP%]   .values[_ngcontent-%COMP%]   span[_ngcontent-%COMP%] {\n  margin-left: 8px;\n}\n.main-container[_ngcontent-%COMP%]   .parent[_ngcontent-%COMP%]   .child2[_ngcontent-%COMP%]   .device-type[_ngcontent-%COMP%] {\n  color: #9fd769;\n}\n@media only screen and (max-width: 768px) {\n  .main-container[_ngcontent-%COMP%] {\n    display: flex;\n    align-items: center;\n    justify-content: center;\n  }\n  .main-container[_ngcontent-%COMP%]   .donut-chart[_ngcontent-%COMP%] {\n    display: grid;\n    align-content: center;\n    justify-content: center;\n  }\n  .main-container[_ngcontent-%COMP%]   .area-chart[_ngcontent-%COMP%] {\n    margin-top: -150px;\n    display: grid;\n    align-content: center;\n    justify-content: center;\n  }\n  .main-container[_ngcontent-%COMP%]   .parent[_ngcontent-%COMP%] {\n    padding-right: 5px;\n    padding-left: 5px;\n    margin-top: 10%;\n    margin-bottom: 3%;\n    display: flex;\n    justify-content: space-around;\n  }\n  .main-container[_ngcontent-%COMP%]   .parent[_ngcontent-%COMP%]   .child1[_ngcontent-%COMP%] {\n    margin-right: 10%;\n    width: auto;\n  }\n  .main-container[_ngcontent-%COMP%]   .parent[_ngcontent-%COMP%]   .child2[_ngcontent-%COMP%] {\n    margin-left: 10%;\n    width: auto;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxjb250ZW5lZG9yLWFsY2FuY2UuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxXQUFBO0FBQ0Y7QUFDRTtFQUNFLGFBQUE7RUFDQSxzQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7QUFDSjtBQUNJO0VBQ0UsZ0JBQUE7RUFDQSxjQUFBO0FBQ047QUFDSTtFQUNFLGdCQUFBO0VBQ0EsY0FBQTtBQUNOO0FBRUU7RUFDRSxhQUFBO0VBQ0EscUJBQUE7RUFDQSx1QkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtBQUFKO0FBR0U7RUFDRSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxxQkFBQTtFQUNBLHVCQUFBO0FBREo7QUFJRTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtFQUNBLFVBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0FBRko7QUFHSTtFQUNFLFVBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtBQUROO0FBRU07RUFDRSxlQUFBO0VBQ0EsbUJBQUE7QUFBUjtBQUVNO0VBQ0UsYUFBQTtFQUNBLG1CQUFBO0FBQVI7QUFDUTtFQUNFLGVBQUE7RUFDQSxjQUFBO0FBQ1Y7QUFDUTtFQUNFLGVBQUE7RUFDQSxjQUFBO0FBQ1Y7QUFDUTtFQUNFLGlCQUFBO0FBQ1Y7QUFHSTtFQUNFLGlCQUFBO0VBQ0EsVUFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtBQUROO0FBRU07RUFDRSxlQUFBO0VBQ0EsbUJBQUE7QUFBUjtBQUVNO0VBQ0UsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7QUFBUjtBQUNRO0VBQ0UsZUFBQTtFQUNBLGNBQUE7QUFDVjtBQUNRO0VBQ0UsZUFBQTtFQUNBLGNBQUE7QUFDVjtBQUNRO0VBQ0UsZ0JBQUE7QUFDVjtBQUVNO0VBQ0UsY0FBQTtBQUFSO0FBTUE7RUFDRTtJQUNFLGFBQUE7SUFDQSxtQkFBQTtJQUNBLHVCQUFBO0VBSEY7RUFJRTtJQUNFLGFBQUE7SUFDQSxxQkFBQTtJQUNBLHVCQUFBO0VBRko7RUFJRTtJQUNFLGtCQUFBO0lBQ0EsYUFBQTtJQUNBLHFCQUFBO0lBQ0EsdUJBQUE7RUFGSjtFQUlFO0lBQ0Usa0JBQUE7SUFDQSxpQkFBQTtJQUNBLGVBQUE7SUFDQSxpQkFBQTtJQUVBLGFBQUE7SUFFQSw2QkFBQTtFQUZKO0VBR0k7SUFDRSxpQkFBQTtJQUNBLFdBQUE7RUFETjtFQUdJO0lBQ0UsZ0JBQUE7SUFDQSxXQUFBO0VBRE47QUFDRiIsImZpbGUiOiJjb250ZW5lZG9yLWFsY2FuY2UuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubWFpbi1jb250YWluZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICB3aWR0aDogMTAwJTtcblxuICAuaGVhZGVyLWNvbnRhaW5lciB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBtYXJnaW4tdG9wOiAxMTBweDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG5cbiAgICAudGl0bGUge1xuICAgICAgZm9udC1zaXplOiAxLjJlbTtcbiAgICAgIGNvbG9yOiByZ2IoMTQ4LCAxNTMsIDE1OCk7XG4gICAgfVxuICAgIC50b3RhbC12YWx1ZSB7XG4gICAgICBmb250LXNpemU6IDEuNmVtO1xuICAgICAgY29sb3I6IHJnYig1NSwgNTcsIDU5KTtcbiAgICB9XG4gIH1cbiAgLmRvbnV0LWNoYXJ0IHtcbiAgICBkaXNwbGF5OiBncmlkO1xuICAgIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgei1pbmRleDogMTtcbiAgfVxuXG4gIC5hcmVhLWNoYXJ0IHtcbiAgICBtYXJnaW4tdG9wOiAtMTU1cHg7XG4gICAgZGlzcGxheTogZ3JpZDtcbiAgICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIH1cblxuICAucGFyZW50IHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgd2lkdGg6IDcwJTtcbiAgICBtYXJnaW4tcmlnaHQ6IDE1JTtcbiAgICBtYXJnaW4tbGVmdDogMTUlO1xuICAgIG1hcmdpbi10b3A6IDUlO1xuICAgIG1hcmdpbi1ib3R0b206IDMlO1xuICAgIC5jaGlsZDEge1xuICAgICAgd2lkdGg6IDUwJTtcbiAgICAgIG1hcmdpbi1yaWdodDogMTAwcHg7XG4gICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICAgIC50YWJsZXQtdHlwZSB7XG4gICAgICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgICAgIH1cbiAgICAgIC52YWx1ZXMge1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgICAucGVyY2VudCB7XG4gICAgICAgICAgZm9udC1zaXplOiAxcmVtO1xuICAgICAgICAgIGNvbG9yOiByZ2IoNTUsIDU3LCA1OSk7XG4gICAgICAgIH1cbiAgICAgICAgLmRldmljZS1hbW91bnQge1xuICAgICAgICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgICAgICAgICBjb2xvcjogcmdiKDE1NywgMTY0LCAxNzIpO1xuICAgICAgICB9XG4gICAgICAgIHNwYW4ge1xuICAgICAgICAgIG1hcmdpbi1yaWdodDogOHB4O1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICAgIC5jaGlsZDIge1xuICAgICAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gICAgICB3aWR0aDogNTAlO1xuICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgICAuc21hcnRwaG9uZS10eXBlIHtcbiAgICAgICAgZm9udC1zaXplOiAxcmVtO1xuICAgICAgICBmb250LXdlaWdodDogYm9sZGVyO1xuICAgICAgfVxuICAgICAgLnZhbHVlcyB7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xuICAgICAgICAucGVyY2VudCB7XG4gICAgICAgICAgZm9udC1zaXplOiAxcmVtO1xuICAgICAgICAgIGNvbG9yOiByZ2IoNTUsIDU3LCA1OSk7XG4gICAgICAgIH1cbiAgICAgICAgLmRldmljZS1hbW91bnQge1xuICAgICAgICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgICAgICAgICBjb2xvcjogcmdiKDE1NywgMTY0LCAxNzIpO1xuICAgICAgICB9XG4gICAgICAgIHNwYW4ge1xuICAgICAgICAgIG1hcmdpbi1sZWZ0OiA4cHg7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICAgIC5kZXZpY2UtdHlwZSB7XG4gICAgICAgIGNvbG9yOiAjOWZkNzY5O1xuICAgICAgfVxuICAgIH1cbiAgfVxufVxuXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDc2OHB4KSB7XG4gIC5tYWluLWNvbnRhaW5lciB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIC5kb251dC1jaGFydCB7XG4gICAgICBkaXNwbGF5OiBncmlkO1xuICAgICAgYWxpZ24tY29udGVudDogY2VudGVyO1xuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgfVxuICAgIC5hcmVhLWNoYXJ0IHtcbiAgICAgIG1hcmdpbi10b3A6IC0xNTBweDtcbiAgICAgIGRpc3BsYXk6IGdyaWQ7XG4gICAgICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICB9XG4gICAgLnBhcmVudCB7XG4gICAgICBwYWRkaW5nLXJpZ2h0OiAgNXB4O1xuICAgICAgcGFkZGluZy1sZWZ0OiA1cHg7XG4gICAgICBtYXJnaW4tdG9wOiAxMCU7XG4gICAgICBtYXJnaW4tYm90dG9tOiAzJTtcbiAgICAgIGRpc3BsYXk6IC13ZWJraXQtZmxleDtcbiAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAtd2Via2l0LWp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kOyBcbiAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYXJvdW5kO1xuICAgICAgLmNoaWxkMXtcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiAxMCU7XG4gICAgICAgIHdpZHRoOiBhdXRvO1xuICAgICAgfVxuICAgICAgLmNoaWxkMntcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDEwJTtcbiAgICAgICAgd2lkdGg6IGF1dG87XG4gICAgICB9XG4gICAgfVxuICB9XG59XG4iXX0= */"]
      });
      /***/
    },

    /***/
    "zNhO":
    /*!*******************************************************************!*\
      !*** ./src/app/components/grafico-dona/grafico-dona.component.ts ***!
      \*******************************************************************/

    /*! exports provided: GraficoDonaComponent */

    /***/
    function zNhO(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "GraficoDonaComponent", function () {
        return GraficoDonaComponent;
      });
      /* harmony import */


      var angular_google_charts__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! angular-google-charts */
      "icpI");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");

      var GraficoDonaComponent = /*#__PURE__*/function () {
        function GraficoDonaComponent() {
          _classCallCheck(this, GraficoDonaComponent);

          this.type = angular_google_charts__WEBPACK_IMPORTED_MODULE_0__["ChartType"].PieChart;
          this.width = 0;
          this.height = 300;
          this.width = screen.width;
        }

        _createClass(GraficoDonaComponent, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            console.log(this.options);
            this.donutChart = {
              type: this.type,
              data: this.options.data,
              options: {
                pieHole: 0.9,
                backgroundColor: 'none',
                legend: 'none',
                pieSliceText: 'none',
                colors: this.options.colors
              }
            };
          }
        }]);

        return GraficoDonaComponent;
      }();

      GraficoDonaComponent.ɵfac = function GraficoDonaComponent_Factory(t) {
        return new (t || GraficoDonaComponent)();
      };

      GraficoDonaComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
        type: GraficoDonaComponent,
        selectors: [["app-grafico-dona"]],
        inputs: {
          options: "options"
        },
        decls: 2,
        vars: 5,
        consts: [[3, "type", "data", "options", "width", "height"], ["chart", ""]],
        template: function GraficoDonaComponent_Template(rf, ctx) {
          if (rf & 1) {
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "google-chart", 0, 1);
          }

          if (rf & 2) {
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("type", ctx.donutChart.type)("data", ctx.donutChart.data)("options", ctx.donutChart.options)("width", ctx.width)("height", ctx.height);
          }
        },
        directives: [angular_google_charts__WEBPACK_IMPORTED_MODULE_0__["GoogleChartComponent"]],
        styles: [".donut-chart[_ngcontent-%COMP%] {\n  display: grid;\n  align-content: center;\n  justify-content: center;\n  position: relative;\n  z-index: 1;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXC4uXFxncmFmaWNvLWRvbmEuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxhQUFBO0VBQ0EscUJBQUE7RUFDQSx1QkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtBQUNKIiwiZmlsZSI6ImdyYWZpY28tZG9uYS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5kb251dC1jaGFydCB7XHJcbiAgICBkaXNwbGF5OiBncmlkO1xyXG4gICAgYWxpZ24tY29udGVudDogY2VudGVyO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB6LWluZGV4OiAxO1xyXG4gIH0iXX0= */"]
      });
      /***/
    },

    /***/
    "zUnb":
    /*!*********************!*\
      !*** ./src/main.ts ***!
      \*********************/

    /*! no exports provided */

    /***/
    function zUnb(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony import */


      var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/platform-browser */
      "jhN1");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "fXoL");
      /* harmony import */


      var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./app/app.module */
      "ZAI4");
      /* harmony import */


      var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./environments/environment */
      "AytR");

      if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
      }

      _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["platformBrowser"]().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])["catch"](function (err) {
        return console.error(err);
      });
      /***/

    },

    /***/
    "zn8P":
    /*!******************************************************!*\
      !*** ./$$_lazy_route_resource lazy namespace object ***!
      \******************************************************/

    /*! no static exports found */

    /***/
    function zn8P(module, exports) {
      function webpackEmptyAsyncContext(req) {
        // Here Promise.resolve().then() is used instead of new Promise() to prevent
        // uncaught exception popping up in devtools
        return Promise.resolve().then(function () {
          var e = new Error("Cannot find module '" + req + "'");
          e.code = 'MODULE_NOT_FOUND';
          throw e;
        });
      }

      webpackEmptyAsyncContext.keys = function () {
        return [];
      };

      webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
      module.exports = webpackEmptyAsyncContext;
      webpackEmptyAsyncContext.id = "zn8P";
      /***/
    }
  }, [[0, "runtime", "vendor"]]]);
})();
//# sourceMappingURL=main-es5.js.map